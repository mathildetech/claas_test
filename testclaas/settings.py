# -*- coding:utf-8 -*-
import os
import logging

EMAIL = {
    'from': 'info@mathildetech.com',
    #'recipients': ["devs@alauda.io"]
    'recipients': ["ops@alauda.io"]
}

NAMESPACE = os.getenv("JAKIRO_USERNAME")
PASSWORD = os.getenv("JAKIRO_PASSWORD")
API_TOKEN = os.getenv("JAKIRO_TOKEN")
EXEC_ENDPOINT = os.getenv("EXEC_ENDPOINT")
REGION_NAME = os.getenv("REGION_NAME")
CLAAS_NAMESPACE = os.getenv("REGION_NAMESPACE")
EXEC_REGION_NAME = REGION_NAME if EXEC_ENDPOINT != None else ''
EXEC_FEATURE = {EXEC_REGION_NAME: EXEC_ENDPOINT}
CLASS_FEATURE = ["application", "template", "alarm", "bridge-network", "host-network", "raw-container"]#支持的功能
SERVICE_CLAAS_REGION = [REGION_NAME]

#非必填项
#API_URL = 'http://api-int.alauda.cn/'
API_URL = 'https://api.alauda.cn/'
LUCIFER_URL = 'http://lucifer-int.alauda.cn:8083/'
REGISTRY_URL = 'index.alauda.cn'
SMTP = {
        'host': os.getenv('SMTP_HOST', 'smtp.mailgun.org'),
        'port': os.getenv('SMTP_PORT', 465),
        'username': os.getenv('SMTP_USERNAME',
                              'postmaster@sandbox9fe5d7261040492baf1e4e9313bb31b5.mailgun.org'),
        'password': os.getenv('SMTP_PASSWORD', '194caaf3af3284aa531b247797db356a'),
        'debug_level': 'DEBUG'
}
logging.basicConfig(
    level=logging.DEBUG,
    format=('%(asctime)s [%(process)d] %(levelname)s %(pathname)s' +
            ' %(funcName)s Line:%(lineno)d %(message)s'),
    filename=os.getenv('LOG_PATH', '/tmp/e2e.log'),
)
