from time import strftime, localtime, time, sleep
import sys
import os
import json
import logging
import settings
import threading
from utils import send_email

logger = logging.getLogger()


def test_task():
    mutex = threading.Lock()
    resultdata = {}
    threads = []

    test_list = os.listdir("./CLaaS")
    for test_file in test_list:
        if test_file[-3:] == '.py' and test_file != '__init__.py':
            n = test_file[0:-3]   # filename
            exec "import {}.{}".format("CLaaS", n)
            amod = sys.modules["CLaaS" + "." + n]
            print amod
            for region in amod.REGIONS:
                param = {"name": n, "region": region}
                evalstr = "CLaaS" + "." + n + ".alaudatest(" + json.dumps(param)\
                    + ", mutex, resultdata)"
                print evalstr, mutex
                threads.append(eval(evalstr))

    for t in threads:
        t.start()
        sleep(40)

    for t in threads:
        t.join()

    print "*********** result ***********"
    print resultdata
    logger.info(json.dumps(resultdata))
    is_success = 'Success'
    ot = {}
    cols = set()
    allregions = set()
    errcode = []
    # errindex = 0
    for result_type in resultdata:
        for region in resultdata[result_type]:
            allregions.add(region)
            obj = resultdata[result_type][region]
            if True:  # obj["success"]:
                for key in obj:
                    if key == "report" or key == "success":
                        continue
                    row = result_type + " " + key
                    if row not in ot:
                        ot[row] = {}
                    if isinstance(obj[key], str):
                        ot[row][region] = obj[key]
                        is_success = 'Failed'
                    elif isinstance(obj[key], int) or\
                            isinstance(obj[key], float):
                        ot[row][region] = round(obj[key], 2)
                    else:
                        ot[row][region] = obj[key]
                        is_success = 'Failed'
                    cols.add(row)
            # else:
            #    if type not in ot:
            #        ot[result_type] = {}
            #    errcode.append(obj["message"])
            #    is_success = 'Failed'
            #    ot[result_type][region] = "errmsg_{}".format(errindex + 1)
            #    errindex = errindex + 1
            #    cols.add(result_type)
    print "*********** result ***********"
    cols = sorted(cols)
    cols.reverse()
    allregions = sorted(allregions)
    print ot

    print "********* create html email body *********"
    html = '<table border="1"><tr><th>&nbsp;</th>'
    for region in allregions:
        html = html + "<th>{}</th>".format(region)
    html = html + "</tr>"
    for clos_type in cols:
        html = html + "<tr><th>{}</th>".format(clos_type)
        for region in allregions:
            if clos_type in ot and region in ot[clos_type]:
                if isinstance(ot[clos_type][region], str):
                    html = html + "<td style='color:red;'>{}</td>"\
                        .format(ot[clos_type][region])
                else:
                    html = html + "<td align='right' >{}</td>"\
                        .format(ot[clos_type][region])
            else:
                html = html + "<td>&nbsp;</td>"
        html = html + "</tr>"
    html = html + "</table>"

    errhtml = ""
    if len(errcode) > 0:
        errhtml = '<div>Error Code:</div><table border="1">'
        for index, item in enumerate(errcode):
            errhtml = errhtml + "<tr><td>errmsg_{}&nbsp;</td><td>&nbsp;{}</td>\
                </tr>".format(index + 1, item)
        errhtml = errhtml + "</table>"

    body = "<html><head></head><body>"
    body = body + strftime("%Y-%m-%d %H:%M:%S", localtime(time()+28800.0))
    body = body + errhtml + html
    body = body + "</body></html>"

    if True:
        env = "CLaasDeploy"
        send_email("[{}] ({}) End-to-End Test for {}".format(is_success, env, "CLaaS"),
                   body,
                   settings.EMAIL['recipients'])
    logger.info("======================================\r\n\r\n\r\n")


if __name__ == "__main__":
    logger.info("\r\n\r\n\r\n======================================")
    test_task()
    logger.info("======================================\r\n\r\n\r\n")
