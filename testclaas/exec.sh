#!/bin/bash
passwd=$5
/usr/bin/expect <<-EOF
set timeout 360
spawn /usr/bin/ssh -o StrictHostKeyChecking=no -t $4@$1 -p 4022 $3/$2 /bin/ls / 
expect {
"*password:" { send "$passwd\r" }
}
expect "*#"
EOF
