from time import time
import sys
sys.path.append("..")
from utils import TestThread
import logging
import settings
import requests
import json

logger = logging.getLogger()
REGIONS = settings.SERVICE_CLAAS_REGION
headers = {
    "Authorization": "Token " + settings.API_TOKEN,
    'User-Agent': 'rubick/v1.0'
}


def create_templates(template_name):
    time1 = time()
    url = "{}v1/application-templates/{}".format(settings.API_URL, settings.CLAAS_NAMESPACE)
    data = {"namespace": "testorg001", "name": template_name, "template": "web:\n  image: index-staging.alauda.cn/alaluda/hello-world:latest", "description": ""}
    print data, type(data)
    files =  {'template': ('compose.yaml', 'web:\n  image: index-staging.alauda.cn/alaluda/hello-world:latest')}
    r = requests.post(url, data=data, files=files, headers=headers)
    print r.text
    if r.status_code != 201:
        return {"success":False, "total": "create template return code error: {}".format(r.status_code)}
    time2 = time()
    return {"success": True, "total": time2-time1}


def update_templates(template_name):
    time1 = time()
    url = "{}v1/application-templates/{}/{}".format(settings.API_URL, settings.CLAAS_NAMESPACE, template_name)
    payload = {"namespace": "testorg001", "name": template_name, "template": "web:\n  image: index-staging.alauda.cn/alaluda/hello-world:latest", "description": "update template"}
    files =  {'template': ('compose.yaml', 'web:\n  image: index-staging.alauda.cn/alaluda/hello-world:latest')}
    r = requests.put(url, data=payload, files=files, headers=headers)
    if r.status_code != 200:
        return {"success":False, "total": "update template return code error: {}".format(r.status_code)}
    r = requests.get(url, headers=headers)
    if "description" in json.loads(r.text):
        description = json.loads(r.text)['description']
        if description == "update template":
            time2 = time()
            return {
                "success": True,
                "total": time2 - time1
            }
        else:
            return {"success": False, "total": "update message error"}
    else:
        return {"success": False, "total": "description not in template"}


def get_templates(template_name):
    time1 = time()
    url = "{}v1/application-templates/{}/{}".format(settings.API_URL, settings.CLAAS_NAMESPACE, template_name)
    r = requests.get(url, headers=headers)
    if r.status_code != 200:
        return {"success":False, "total": "get template return code error: {}".format(r.status_code)}
    if "name" in json.loads(r.text):
        name = json.loads(r.text)['name']
        if name == template_name:
            time2 = time()
            return {
                "success": True,
                "total": time2 - time1
            }
        else:
            return {"success": False, "total": "template_name is error"}
    else:
        return {"success": False, "total": "template name not in template"}


def delete_templates(template_name):
    time1 = time()
    url = "{}v1/application-templates/{}/{}".format(settings.API_URL, settings.CLAAS_NAMESPACE, template_name)
    r = requests.delete(url, headers=headers)
    if r.status_code != 204:
        return {"success":False, "total": "delete template return code error: {}".format(r.status_code)}
    time2 = time()
    return {"success": True, "total": time2-time1}


class alaudatest(TestThread):
    def __init__(self, param, mutext, result):
        TestThread.__init__(self, param, mutext, result)

    def test(self):
        obj = {}
        template_name ="template{}".format(int(time()))
        if "template" in settings.CLASS_FEATURE:
            ret1 = create_templates(template_name)
            if not ret1['success']:
                return ret1
    
            ret2 = update_templates(template_name)
            if not ret2['success']:
                return ret2
    
            ret3 = get_templates(template_name)
    
            ret4 = delete_templates(template_name)
            
            obj = {
                "success": True,
                "create templates": ret1["total"],
                "update templates": ret2["total"],
                "get templates": ret3["total"],
                "delete templates": ret4["total"],
            }

        return obj
