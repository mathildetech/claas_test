import sys
sys.path.append("..")
from utils import TestThread
import logging
import settings
import data
from common import test_process

logger = logging.getLogger()
REGIONS = settings.SERVICE_CLAAS_REGION


class alaudatest(TestThread):
    def __init__(self, param, mutext, result):
        TestThread.__init__(self, param, mutext, result)

    def test(self):
        obj = {}
        service_name = "test"
        namespace = settings.CLAAS_NAMESPACE
        mode = ''
        for i in range(0, len(settings.CLASS_FEATURE)):
            if "lb" in settings.CLASS_FEATURE[i]:
                mode = settings.CLASS_FEATURE[i]
        if mode:
            serviceData = data.ServiceData(service_name, namespace, self.region, mode.upper())
            if "bridge-network" in settings.CLASS_FEATURE:
                result = test_process(serviceData.elb_bridge_internal_service(), self.region)
                obj.update(result)
                print obj
                result = test_process(serviceData.elb_bridge_external_service(), self.region)
                obj.update(result)
                print obj
        else:
            serviceData = data.ServiceData(service_name, namespace, self.region)
        if "haproxy" in settings.CLASS_FEATURE:
            if "bridge-network" in settings.CLASS_FEATURE:
                result = test_process(serviceData.haproxy_bridge_service(), self.region)
                obj.update(result)
                print obj
        if "raw-container" in settings.CLASS_FEATURE:
            if "bridge-network" in settings.CLASS_FEATURE:
                result = test_process(serviceData.rawcontainer_bridge_service(), self.region)
                obj.update(result)
                print obj
        return obj
