import sys
sys.path.append("..")
from time import time, sleep
import requests
import json
import settings
import logging
from utils import TestThread, apicall_application

logger = logging.getLogger()
REGIONS = settings.SERVICE_CLAAS_REGION

headers = {
    "Authorization": "Token " + settings.API_TOKEN,
    'content-type': 'application/json'
}


def create_application(name, files_data='', region='BEIJING1'):
    time1 = time()
    files = {'services': ('compose.yml', unicode(files_data))}
    (code, text) = apicall_application('create_app', name, region_name=region,
                                       files=files)
    print ("application apicall returns (%d, %s)" % (code, text))
    if code < 200 or code > 300:
        msg = "Error in call create application API {}:{}".format(code, text)
        return {"success": False, "total": msg, "message": msg}
    app_url = "{}v1/applications/{}/{}".format(settings.API_URL,
                                               settings.CLAAS_NAMESPACE, name)
    isdeploy = True
    cnt = 0
    while cnt < 120 and isdeploy:
        cnt = cnt + 1
        r = requests.get(app_url, headers=headers)
        if r.status_code > 300 or r.status_code < 200:
            msg = "Get application status_code error: {}".format(r.status_code)
            return {"success": False, "total": msg}
        if json.loads(r.text)['current_status'] != "Running":
            sleep(3)
            continue
        isdeploy = False
    if isdeploy:
        msg = "Timeout in creating"
        return {"success": False, "total": msg}
    time2 = time()
    print "create success"
    return {
        "success": True,
        "total": time2 - time1
    }


def start_application(name, region='BEIJING1'):
    logger.info(name + " start application")
    time1 = time()
    (code, text) = apicall_application('start_app', name, region_name=region)
    logger.info(name + " apicall returns (%d, %s)" % (code, text))
    print code, text
    if code < 200 or code >= 300:
        msg = "Error in call start application API {}:{}".format(code, text)
        return {"success": False, "total": msg}

    app_url = "{}v1/applications/{}/{}".format(settings.API_URL,
                                               settings.CLAAS_NAMESPACE, name)
    isdeploy = True
    cnt = 0
    while cnt < 120 and isdeploy:
        cnt = cnt + 1
        r = requests.get(app_url, headers=headers)
        print json.loads(r.text)['current_status']
        if r.status_code > 300 or r.status_code < 200:
            msg = "Get application status_code error: {}".format(r.status_code)
            return {"success": False, "total": msg}
        if json.loads(r.text)['current_status'] == "PartialRunning":
            return {"success": False, "total": "application is PartialRunning"}
        if json.loads(r.text)['current_status'] != "Running":
            sleep(3)
            continue
        isdeploy = False
    time2 = time()
    if isdeploy:
        msg = "timeout in create application"
        return {"success": False, "total": msg}

    url = settings.API_URL + "v1/services/{}/web?application={}".\
        format(settings.CLAAS_NAMESPACE, name)
    r = requests.get(url, headers=headers)
    if r.status_code != 200:
        msg = "error code:{},error text:{}".format(r.status_code, r.text)
        return {"success": False, "total": msg}
    domain_name = json.loads(r.text)['instance_ports'][0]['default_domain']
    web_url = "http://{}".format(domain_name)
    print web_url
    try:
        r = requests.get(web_url)
    except Exception as e:
        return {"success": False, "total": "failed in get web "}
    cnt = 0
    success = False
    while cnt < 120:
        cnt = cnt + 1
        if r.status_code != 200 or 'Hello World!' not in r.text:
            r = requests.get(web_url)
            sleep(3)
            continue
        success = True
        break
    if not success:
        return {"success": False, "total": "failed in get web "}
        time2 = time()
    obj = {
        "success": success,
        "total": time2 - time1
    }
    return obj


def write_stop_application(name, token, region='BEIJING1'):
    logger.info(name + " stop application")
    time1 = time()
    (code, text) = apicall_application('stop_app', name, region_name=region, token=token)
    logger.info(name + " apicall returns (%d, %s)" % (code, text))
    if code < 200 or code >= 300:
        msg = "Error in call stop application API {}:{}".format(code, text)
        return {"success": False, "total": msg}

    app_url = "{}v1/applications/{}/{}".format(settings.API_URL,
                                               settings.CLAAS_NAMESPACE, name)
    isdeploy = True
    cnt = 0
    while cnt < 120 and isdeploy:
        cnt = cnt + 1
        r = requests.get(app_url, headers=headers)
        print json.loads(r.text)['current_status']
        if r.status_code > 300 or r.status_code < 200:
            msg = "Get application status_code error: {}".format(r.status_code)
            return {"success": False, "total": msg}
        if json.loads(r.text)['current_status'] != "Stopped":
            sleep(3)
            continue
        isdeploy = False
    time2 = time()
    if isdeploy:
        msg = "Timeout in stopping"
        return {"success": False, "total": msg}
    obj = {
        "success": True,
        "total": time2 - time1
    }
    return obj


def read_stop_application(name, token, region='BEIJING1'):
    logger.info(name + " stop application")
    time1 = time()
    (code, text) = apicall_application('stop_app', name, region_name=region, token=token)
    logger.info(name + " apicall returns (%d, %s)" % (code, text))
    if code != 403:
        msg = "Error in call stop application API {}:{}".format(code, text)
        return {"success": False, "total": msg}

    time2 = time()
    obj = {
        "success": True,
        "total": time2 - time1
    }
    return obj


def delete_application(name, region='BEIJING1'):
    logger.info(region + " Start delete {}".format(name))

    time1 = time()
    (code, text) = apicall_application("delete_app", name, region_name=region)
    if code < 200 or code >= 300:
        msg = "Error in calling delete API {}:{}".format(code, text)
        return {"success": False, "total": msg}
    print code, text
    app_url = "{}v1/applications/{}/{}".format(settings.API_URL,
                                               settings.CLAAS_NAMESPACE, name)
    cnt = 0
    delete_status = False
    while cnt < 120 and not delete_status:
        cnt = cnt + 1
        r = requests.get(app_url, headers=headers)
        print r.status_code
        if r.status_code <= 300 and r.status_code >= 200:
            sleep(3)
            continue
        delete_status = True
    if not delete_status:
        return {"success": False, "total": "delete application failed"}
    time2 = time()
    sleep(10)

    obj = {"success": True, "total": time2 - time1}
    return obj


def get_event(app_name):
    logger.info(" Start get application event {}".format(app_name))
    time1 = time()
    get_app_url = "{}v1/applications/{}/{}".format(settings.API_URL, settings.CLAAS_NAMESPACE, app_name)
    r = requests.get(get_app_url, headers=headers)
    if r.status_code != 200:
        return{"success": False, "total": "get application failed"}
    app_id = json.loads(r.text)['uuid']
    start_time = int(time1) - 60 * 60
    end_time = int(time1) + 1 * 60
    get_event_url = "{}v1/events/{}/application/{}/?start_time={}&end_time={}&pageno=1&size=20".\
        format(settings.API_URL, settings.CLAAS_NAMESPACE, app_id, start_time, end_time)
    r = requests.get(get_event_url, headers=headers)
    if r.status_code != 200:
        return{"success": False, "total": "get event failed"}
    operation = []
    for i in range(0, len(json.loads(r.text)['results'])):
        operation.append(json.loads(r.text)['results'][i]['detail']['operation'])
    if 'start' not in operation:
        return{"success": False, "total": "start not in event"}
    if 'stop' not in operation:
        return{"success": False, "total": "stop not in event"}
    if 'create' not in operation:
        return{"success": False, "total": "create not in event"}
    time2 = time()
    return {"success": True, "total": time2 - time1}


class alaudatest(TestThread):
    def __init__(self, name, mutext, result):
        TestThread.__init__(self, name, mutext, result)

    def test(self):

        time1 = time()
        obj = {}
        internal_name = "test{}".format(int(time1))
        if "application" in settings.CLASS_FEATURE:
            internal_data = "redis:\n image: {}/library/redis:latest\n size: XS\n ports:\n - '6379'\nweb:\n command: python app.py\n image: {}/alauda/flask-redis:latest\n size: XS\n links:\n - redis:redis\n ports:\n - '80/http'".format(settings.REGISTRY_URL,settings.REGISTRY_URL)
    
            delete_application(internal_name, self.region)
            sleep(30)
            internal_app = create_application(internal_name, files_data=internal_data, region=self.region)
            if not internal_app['success']:
                delete_application(internal_name, self.region)
                return internal_app
            logger.info(self.region + "Create external app")
            if settings.READ_TOKEN: 
                read_permisson_stop = read_stop_application(internal_name, settings.READ_TOKEN, self.region)
            else:
                read_permisson_stop = {"success": True, "total": 1}
            if settings.WRITE_TOKEN:
                write_permisson_stop = write_stop_application(internal_name, settings.WRITE_TOKEN, self.region)
            else:
                write_permisson_stop = write_stop_application(internal_name, settings.API_TOKEN, self.region)
            start_internal = start_application(internal_name, self.region)
            event = get_event(internal_name)
    
            delete_internal = delete_application(internal_name, self.region)
            obj = {
                "success": True,
                "create external app": internal_app['total'],
                "read_permisson_stop internal": read_permisson_stop['total'],
                "write_permisson_stop internal": write_permisson_stop['total'],
                "start external": start_internal['total'],
                "event": event['total'],
                "delete external": delete_internal['total']
            }

        return obj
