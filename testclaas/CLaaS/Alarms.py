import sys
sys.path.append("..")
from time import time
from utils import TestThread
import logging
import settings
import requests
import json

logger = logging.getLogger()
REGIONS = settings.SERVICE_CLAAS_REGION
headers = {
    "Authorization": "Token " + settings.API_TOKEN,
    'content-type': 'application/json'
}


def create_alarms(alarm_name):
    time1 = time()
    url = "{}v1/alarms/{}".format(settings.API_URL, settings.CLAAS_NAMESPACE)
    payload = {
        "alarm_actions": {"notifications": []},
        "resource_name": settings.SERVICE_CLAAS_REGION[0], 
        "metric_name": "region-nodes-count", 
        "evaluation_periods": 1, 
        "description": "", 
        "namespace": settings.CLAAS_NAMESPACE, 
        "period": 60, 
        "actions_enabled": True, 
        "dimensions": [], 
        "statistic": "Average", 
        "threshold": 5, 
        "threshold_display": 5, 
        "comparison_operator": "GreaterThanOrEqualToThreshold", 
        "resource_type": "REGION", 
        "name": alarm_name,
    }
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    print r.text
    if r.status_code != 201:
        return {"success":False, "total": "create alarm return code error: {}".format(r.status_code)}
    time2 = time()
    return {"success": True, "total": time2-time1}


def update_alarms(alarm_name):
    time1 = time()
    url = "{}v1/alarms/{}/{}".format(settings.API_URL, settings.CLAAS_NAMESPACE, alarm_name)
    payload = {
        "alarm_actions": {"notifications": []},
        "resource_name": "STAGING", 
        "metric_name": "region-nodes-count", 
        "evaluation_periods": 1, 
        "description": "update alarm", 
        "namespace": settings.CLAAS_NAMESPACE, 
        "period": 60, 
        "actions_enabled": True, 
        "dimensions": [], 
        "statistic": "Average", 
        "threshold": 5, 
        "threshold_display": 5, 
        "comparison_operator": "GreaterThanOrEqualToThreshold", 
        "resource_type": "REGION", 
        "name": alarm_name,
    }
    r = requests.put(url, data=json.dumps(payload), headers=headers)
    if r.status_code != 204:
        return {"success":False, "total": "update alarm return code error: {}".format(r.status_code)}
    r = requests.get(url, headers=headers)
    if "description" in json.loads(r.text):
        description = json.loads(r.text)['description']
        if description == "update alarm":
            time2 = time()
            return {
                "success": True,
                "total": time2 - time1
            }
        else:
            return {"success": False, "total": "update message error"}
    else:
        return {"success": False, "total": "description not in alarm"}


def get_alarms(alarm_name):
    time1 = time()
    url = "{}v1/alarms/{}/{}".format(settings.API_URL, settings.CLAAS_NAMESPACE, alarm_name)
    r = requests.get(url, headers=headers)
    if r.status_code != 200:
        return {"success":False, "total": "get alarm return code error: {}".format(r.status_code)}
    if "resource_name" in json.loads(r.text):
        resource_name = json.loads(r.text)['resource_name']
        if resource_name == settings.SERVICE_CLAAS_REGION[0]:
            time2 = time()
            return {
                "success": True,
                "total": time2 - time1
            }
        else:
            return {"success": False, "total": "STAGING not in alarm"}
    else:
        return {"success": False, "total": "resource_name not in alarm"}


def delete_alarms(alarm_name):
    time1 = time()
    url = "{}v1/alarms/{}/{}".format(settings.API_URL, settings.CLAAS_NAMESPACE, alarm_name)
    r = requests.delete(url, headers=headers)
    if r.status_code != 204:
        return {"success":False, "total": "delete alarm return code error: {}".format(r.status_code)}
    time2 = time()
    return {"success": True, "total": time2-time1}


class alaudatest(TestThread):
    def __init__(self, param, mutext, result):
        TestThread.__init__(self, param, mutext, result)

    def test(self):
        obj = {}
        if "alarm" in settings.CLASS_FEATURE:
            alarm_name ="alarm{}".format(int(time()))
            ret1 = create_alarms(alarm_name)
            if not ret1['success']:
                return ret1
    
            ret2 = update_alarms(alarm_name)
            if not ret2['success']:
                return ret2
    
            ret3 = get_alarms(alarm_name)
    
            ret4 = delete_alarms(alarm_name)
    
            obj = {
                "success": True,
                "create alarms": ret1["total"],
                "update alarms": ret2["total"],
                "get alarms": ret3["total"],
                "delete alarms": ret4["total"],
            }

        return obj