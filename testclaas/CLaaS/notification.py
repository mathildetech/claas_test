from time import time
import sys
sys.path.append("..")
from utils import TestThread
import logging
import settings
import requests
import json

logger = logging.getLogger()
REGIONS = settings.SERVICE_CLAAS_REGION
uuid = ''
headers = {
    "Authorization": "Token " + settings.API_TOKEN,
    'content-type': 'application/json'
}
url = settings.LUCIFER_URL


def create_notification():
    time1 = time()
    post_url = url + 'v1/notifications'
    payload = {"subscriptions": [{"method": "email", "recipient": "dongxu@alauda.io"}]}
    print post_url
    r = requests.post(post_url, data=json.dumps(payload), headers=headers)
    print r.status_code, r.text
    if r.status_code != 201:
        return {"success": False, "total": "create notification error in call lucifer api"}
    if 'uuid' in json.loads(r.text):
        global uuid
        uuid = json.loads(r.text)['uuid']
    else:
        return {"success": False, "total": "uuid not return after create notification"}
    time2 = time()
    return {
        "success": True,
        "total": time2 - time1
    }


def update_notification():
    time1 = time()
    put_url = url + 'v1/notifications/' + uuid
    payload = {"subscriptions": [{"method": "email", "recipient": "hchan@alauda.io"}]}
    r = requests.put(put_url, data=json.dumps(payload), headers=headers)
    if r.status_code != 200:
        return {"success": False, "total": "update notification error in call lucifer api"}
    time2 = time()
    return {
        "success": True,
        "total": time2 - time1
        }


def get_notification():
    time1 = time()
    get_url = url + 'v1/notifications/' + uuid
    r = requests.get(get_url, headers=headers)
    if r.status_code != 200:
        return {"success": False, "total": "get notification error in call lucifer api"}
    if "subscriptions" in json.loads(r.text):
        email = json.loads(r.text)['subscriptions'][0]['recipient']
        if email == 'hchan@alauda.io':
            time2 = time()
            return {
                "success": True,
                "total": time2 - time1
            }
        else:
            return {"success": False, "total": "email not in notification in call lucifer api"}
    else:
        return {"success": False, "total": "subscriptions not in notification in call lucifer api"}


def delete_notification():
    time1 = time()
    delete_url = url + 'v1/notifications/' + uuid
    r = requests.delete(delete_url, headers=headers)
    if r.status_code != 204:
        return {"success": False, "total": "delete notification error in call lucifer api"}
    r = requests.get(delete_url, headers=headers)
    if 'errors' in json.loads(r.text):
        error_message = json.loads(r.text)['errors'][0]['message']
        if error_message == 'Notification matching query does not exist.':
            time2 = time()
            obj = {
                "success": True,
                "total": time2 - time1
            }
            return obj
        else:
            return {"success": False, "total": "error message is incorrect in call lucifer api"}
    else:
        return {"success": False, "total": "delete  notification failed in call lucifer api"}


class alaudatest(TestThread):
    def __init__(self, param, mutext, result):
        TestThread.__init__(self, param, mutext, result)

    def test(self):
        obj = {}
        if "notification" in settings.CLASS_FEATURE:
            ret1 = create_notification()
            if not ret1['success']:
                return ret1
    
            ret2 = update_notification()
            if not ret2['success']:
                return ret2
    
            ret3 = get_notification()
    
            ret4 = delete_notification()
    
            obj = {
                "success": True,
                "create notification": ret1["total"],
                "update notification": ret2["total"],
                "get notification": ret3["total"],
                "delete notification": ret4["total"],
            }

        return obj
