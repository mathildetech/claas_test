import logging
import sys
sys.path.append("..")
import settings
import data
from common import test_process
from utils import TestThread

logger = logging.getLogger()
REGIONS = settings.SERVICE_CLAAS_REGION


class alaudatest(TestThread):
    def __init__(self, param, mutext, result):
        TestThread.__init__(self, param, mutext, result)

    def test(self):
        obj = {}
        service_name = "test"
        namespace = settings.CLAAS_NAMESPACE
        mode = ''
        for i in range(0, len(settings.CLASS_FEATURE)):
            if "lb" in settings.CLASS_FEATURE[i]:
                mode = settings.CLASS_FEATURE[i]
        if mode:
            serviceData = data.ServiceData(service_name, namespace, self.region, mode.upper())
            if "host-network" in settings.CLASS_FEATURE:
                result = test_process(serviceData.elb_host_internal_service(), self.region)
                obj.update(result)
                print obj
                result = test_process(serviceData.elb_host_external_service(), self.region)
                obj.update(result)
                print obj
        else:
            serviceData = data.ServiceData(service_name, namespace, self.region)
        if "haproxy" in settings.CLASS_FEATURE:
            if "host-network" in settings.CLASS_FEATURE:
                result = test_process(serviceData.haproxy_host_service(), self.region)
                obj.update(result)
                print obj
        if "raw-container" in settings.CLASS_FEATURE:
            if "host-network" in settings.CLASS_FEATURE:
                result = test_process(serviceData.rawcontainer_host_service(), self.region)
                obj.update(result)
                print obj
        return obj
