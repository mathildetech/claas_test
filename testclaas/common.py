from time import sleep, time
from utils import TestThread
import logging
import settings
import json
import data
from utils import is_deploying, apicall_claas_service, get_service_info, can_visit, exec_feature

logger = logging.getLogger()
REGIONS = settings.SERVICE_CLAAS_REGION


def create_service(name, payload, service_detail='Hello', region='BEIJING1'):
    print "create claas service"
    logger.info(region + " Start creating service")

    time1 = time()
    (code, text) = apicall_claas_service('create_service', name, payload)
    logger.info("apicall returns (%d, %s)" % (code, text))
    if code < 200 or code >= 300:
        msg = "Error in calling create Claas API {}:{}".format(code, text)
        return {"success": False, "total": msg}

    # test if at least 1 instance is running
    target = {"status": "Running", "text": service_detail}
    isdeploying = is_deploying(name, target, 200, settings.CLAAS_NAMESPACE)
    time2 = time()
    logger.info(region + " is_deploying check use {}s".format(time2 - time1))
    if isdeploying:
        (code, text, obj) = get_service_info(name, 0, settings.CLAAS_NAMESPACE)
        if 'current_status' in obj:
            if obj['current_status'] == 'Error':
                return {"success": False, "total": "create service {} status is Error".format(name)}
            flag = can_visit(target, text)
            if not flag and obj['current_status'] == 'Running':
                return {"success": False, "total": "create service {} can not visit".format(name)}
        msg = "Timeout in creating {} service".format(name)
        return {"success": False, "total": msg}
    logger.info(region + " create use {}s".format(time2 - time1))

    return {
        "success": True,
        "total": time2 - time1
    }


def delete_app(name, region_name='BEIJING1'):
    logger.info(region_name + " start deleting {}".format(name))

    time1 = time()
    (code, text) = apicall_claas_service('delete_service', name)
    logger.info(region_name + " apicall returns (%d, %s)" % (code, text))
    if code < 200 or code >= 300:
        msg = "Error in calling delete API {}:{}".format(code, text)
        return {"success": False, "total": msg}

    time2 = time()
    logger.info(region_name + " total deletion uses %f seconds"
                % (time2 - time1))
    sleep(10)
    obj = {
        "success": True,
        "total": time2 - time1
    }
    return obj


def get_metrics(name):
    logger.info(" start get service metrics")
    print "get_metrics()"

    time1 = time()
    app_instance = name + "/instances"
    (code, instances_text) = apicall_claas_service('get_service', app_instance)
    if code < 200 or code > 300:
        msg = "Error in get_instances API {}:{}".format(code,
                                                        instances_text)
        return {"success": False, "total": msg}

    cnt = 0
    isdeploying = True
    text = ''
    while cnt < 200 and isdeploying:
        cnt = cnt + 1
        tm_end = int(time())
        tm_start = tm_end-1800 
        interval_time = "/metrics/?start_time=%s&end_time=%s&point_per_period=1m"\
            % (tm_start, tm_end)

        isdeploying = False
        for instance in json.loads(instances_text):
            instance_uuid = instance['uuid']
            instance_metrics = app_instance + "/" +\
                instance_uuid + interval_time
            (code, text) = apicall_claas_service('get_service', instance_metrics)
            logger.info(" get service metrics {} {} '{}'"
                        .format(instance_metrics, code, text))
            if code >= 300 or text == '':
                isdeploying = True
                sleep(2)
                break
            obj = json.loads(text)
            if obj['points'] == []:
                isdeploying = True
                sleep(2)
                break

    if code >= 300:
        msg = "Failed in getting metrics , return {}".format(code)
        return {"success": False, "total": msg}
    if text == '':
        msg = "Failed in getting metrics , text is null"
        return {"success": False, "total": msg}
    if json.loads(text)['points'] == []:
        msg = "Failed in getting metrics , text is []"
        return {"success": False, "total": msg}
    print json.loads(text)['points']
    time2 = time()
    logger.info(" ==> Getting metrics uses {}s"
                .format(time2 - time1))
    obj = {
        "success": True,
        "total": time2 - time1
    }
    return obj

def get_logs(name):
    logger.info(" start get service logs")
    print "get_logs()"

    time1 = time()

    cnt = 0
    is_deploying = True
    logs_code = 200
    logs_text = ''
    while cnt < 200 and is_deploying:
        cnt = cnt + 1
        tm_end = int(time())
        tm_start = tm_end-604800
        interval_time = "/logs/?start_time=%s&end_time=%s" % (tm_start, tm_end)
        is_deploying = False
        instance_logs = name + interval_time
        print "instance logs: "
        print "instance logs:{} ".format(instance_logs)
        (logs_code, logs_text) = apicall_claas_service('get_service', instance_logs)
        print (logs_code, logs_text)
        if logs_code < 200 or logs_code > 300:
            is_deploying = True
            sleep(2)
            break
        if logs_text == '""' or logs_text == '[]':
            is_deploying = True
            sleep(5)
            break
    if logs_code < 200 or logs_code > 300:
        msg = "Failed in getting logs {}".format(logs_code)
        return {"success": False, "total": msg}
    if logs_text == '""' or logs_text == '[]':
        return {"success": False, "total": "log is null"}

    time2 = time()
    logger.info(" total getting logs uses {}s"
                .format(time2 - time1))
    obj = {
        "success": True,
        "total": time2 - time1
    }
    print "get logs success"
    return obj


def start_app(name, num):
    logger.info(" start service")
    print "start_app()"

    time1 = time()
    (code, text) = apicall_claas_service('start_service', name)
    logger.info(" apicall returns (%d, %s)" % (code, text))
    if code < 200 or code >= 300:
        msg = "Error in calling start API {}:{}".format(code, text)
        return {"success": False, "total": msg}

    target = {"status": "Running", "num": num}
    isdeploying = is_deploying(name, target, 180, settings.CLAAS_NAMESPACE)
    time2 = time()
    if isdeploying:
        (code, text, obj) = get_service_info(name, 0, settings.CLAAS_NAMESPACE)
        if 'current_status' in obj:
            if obj['current_status'] == 'Error':
                return {"success": False, "total": "restart {} service status is Error".format(name)}
        return {"success": False, "total": "Timeout in starting {}".format(name)}

    logger.info(" ==> Starting service uses {}s"
                .format(time2 - time1))
    obj = {
        "success": True,
        "total": time2 - time1
    }
    return obj

def stop_app(name):
    logger.info(" Stopping service")
    print "stop_app()"

    time1 = time()
    (code, text) = apicall_claas_service('stop_service', name)
    logger.info(" apicall returns (%d, %s)" % (code, text))
    if code < 200 or code >= 300:
        msg = "Error in calling stop API {}:{}".format(code, text)
        return {"success": False, "total": msg}

    target = {"status": "Stopped", "num": 0}
    isdeploying = is_deploying(name, target, 120, settings.CLAAS_NAMESPACE)
    time2 = time()
    if isdeploying:
        return {"success": False, "total": "Timeout in stopping {}".format(name)}

    logger.info(" ==> Stopping service uses {}s"
                .format(time2 - time1))
    obj = {
        "success": True,
        "total": time2 - time1
    }
    return obj


def update_service(name, num, size, service_detail='Hello'):
    print "scale_app()"
    logger.info(" Start scaling service {}".format(name))

    time1 = time()
    payload = {
          "image_tag": "latest",
          "target_num_instances": num,
          "instance_size": size 
          }
    (code, text) = apicall_claas_service('modify_service', name, payload)
    logger.info(" apicall returns (%d, %s)" % (code, text))
    if code < 200 or code >= 300:
        msg = "Error in calling scale API {}:{}".format(code, text)
        return {"success": False, "total": msg}

    target = {"status": "Running", "num": num, "size": size, "text": service_detail}
    isdeploying = is_deploying(name, target, 240, settings.CLAAS_NAMESPACE)
    time2 = time()
    if isdeploying:
        (code, text, obj) = get_service_info(name, 0, settings.CLAAS_NAMESPACE)
        if 'current_status' in obj:
            if obj['current_status'] == 'Error':
                return {"success": False, "total": "update service {} status is Error".format(name)}
            flag = can_visit(target, text)
            if not flag and obj['current_status'] == 'Running':
                return {"success": False, "total": "update service {} can not visit".format(name)}
        msg = "Timeout in scaling {}".format(name)
        return {"success": False, "total": msg}

    logger.info(" ==> Scaling uses {}s"
                .format(time2 - time1))
    return {
        "success": True,
        "total": time2 - time1
    }


def test_process(data, region, text='Hello'):
    time1 = time()
    app_name = data['service_name']

    delete_app(app_name, region)
    sleep(10)
    ret_create = create_service(app_name, data, service_detail=text, region=region)
    if not ret_create["success"]:
        return {"success": False, "{} total".format(app_name): ret_create["total"]}
    logger.info(region +
                " Creating a service %fs finished\n\n"
                % (ret_create["total"]))
    sleep(200)
    
    ret_metrics = get_metrics(app_name)
    ret_exec = exec_feature(app_name, region, settings.CLAAS_NAMESPACE)
    
    ret_stop = stop_app(app_name)
    if not ret_stop["success"]:
        return {
            "success": False,
            "{} total".format(app_name): ret_stop["total"]
        }
    logger.info(region + " Stop the service %s finished\n\n"
                % ret_stop["total"])
    sleep(10)

    ret_restart = start_app(app_name, num=1)
    if not ret_restart["success"]:
        return {
            "success": False,
            "{} total".format(app_name): ret_restart["total"]
        }
    logger.info(region + " Start the service %s finished\n\n"
                % ret_restart["total"])
    sleep(1)
    
    scale_up = update_service(app_name, num=2, size="XS", service_detail=text)
    if not scale_up["success"]:
        return {
            "success": False,
            "{} total".format(app_name): scale_up["total"]
        }
    scale_down = update_service(app_name, num=1, size="XS", service_detail=text)
    if not scale_down["success"]:
        return {
            "success": False,
            "{} total".format(app_name): scale_down["total"]
        }
    logger.info(region + " Redeploy the service %s finished\n\n"
                % scale_down["total"])

    ret_logs = get_logs(app_name)
    
    logger.info(region + " begin to delete service")
    ret7 = delete_app(app_name, region)
    if not ret7["success"]:
        ret7["total"] = "error in deleting basic service"
    logger.info(region + " Delete the service %s finished\n\n"
                % ret7["total"])


    obj = {
        "success": True,
        "{} create".format(app_name): ret_create["total"],
        "{} get logs".format(app_name): ret_logs["total"],
        "{} get metrics".format(app_name): ret_metrics["total"],
        "{} exec_feature".format(app_name): ret_exec["total"],
        "{} get stop service".format(app_name): ret_stop["total"],
        "{} get restart service".format(app_name): ret_restart["total"],
        "{} scale up service".format(app_name): scale_up["total"],
        "{} scale down service".format(app_name): scale_down["total"],
        "{} delete".format(app_name): ret7["total"],
    }

    return obj
