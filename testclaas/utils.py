from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
import smtplib
import logging
import settings
import requests
import json
import commands
import threading
from time import sleep, time
from settings import EMAIL, SMTP

logger = logging.getLogger()

headers = {
    "Authorization": "Token " + settings.API_TOKEN,
    'content-type': 'application/json'
}


class TestThread(threading.Thread):
    def __init__(self, param, mutex, result):
        threading.Thread.__init__(self)
        self.param = param
        self.name = param['name']
        self.region = param['region']
        self.mutex = mutex
        self.result = result

    def test(self):
        return

    def run(self):
        print "===== " + self.region + " " + self.name + " ====="
        logger.info("===== " + self.region + " " + self.name + " =====")
        ret = self.test()
        self.mutex.acquire()
        if self.name not in self.result:
            self.result[self.name] = {}
        self.result[self.name][self.region] = ret
        self.mutex.release()
        print "===== " + self.region + " " + self.name + " returns ====="
        logger.info("===== " + self.region + " " + self.name + " returns =====")
        print ret


def apicall_claas_service(action, service_name, payload={}):
    url = settings.API_URL + 'v1/services/' + settings.CLAAS_NAMESPACE + '/'
    time1 = time()
    if action == 'create_service':
        r = requests.post(url, data=json.dumps(payload), headers=headers)
    if action == 'modify_service':
        url = url + service_name + "/"
        r = requests.put(url, data=json.dumps(payload), headers=headers)
    if action == 'delete_service':
        url = url + service_name + "/"
        print url, headers
        r = requests.delete(url, headers=headers)
    if action == 'get_service':
        url = url + service_name
        r = requests.get(url, headers=headers)
    if action == 'start_service':
        url = url + service_name + "/start/"
        r = requests.put(url, data=json.dumps(payload), headers=headers)
    if action == 'stop_service':
        url = url + service_name + "/stop/"
        r = requests.put(url, data=json.dumps(payload), headers=headers)
    time2 = time()
    logger.debug("%s %s %s %d  time used: %f"
                 % (url, action, service_name,
                    r.status_code, time2 - time1))
    # print (url, payload, r.status_code, r.text)
    return (r.status_code, r.text)


def exec_feature(name, region, namespace):
    logger.info("start test exec feature, Service is %s" % name)
    print "Test exec feature"

    if region in settings.EXEC_FEATURE:
        cmd = "./exec.sh {} {}.0 {} {} {}".format(settings.EXEC_FEATURE[region], name, namespace, settings.NAMESPACE, settings.PASSWORD)
    else:
        return {"success": True, "total": "this region do not have exec feature"}
    print cmd
    logger.info(cmd)
    time1 = time()
    for i in range(1, 5):
        text_non = commands.getoutput(cmd)
        logger.info(text_non)
        print text_non
        commands_string = "home"
        if text_non.find(commands_string) >= 0:
            break
    if text_non.find(commands_string) == -1:
        logger.info("Exec Feature is not work")
        return {"success": False, "total": "Failed in Exec feature"}
    time2 = time()

    obj = {
        "success": True,
        "total": time2 - time1
    }
    return obj


def apicall_application(action,
                        application_name,
                        region_name='BEIJING1',
                        files={'services': ('compose.yaml', '')},
                        token=settings.API_TOKEN):

    if region_name == 'BEIJING1':
        url = settings.API_URL + 'v1/applications/' + settings.NAMESPACE + '/'
    elif region_name == settings.SERVICE_CLAAS_REGION[0]:
        url = settings.API_URL + 'v1/applications/' + settings.CLAAS_NAMESPACE + '/'
    payload = {
        "app_name": application_name,
        "region": region_name,
        "namespace": settings.NAMESPACE
    }
    headers = {
        "Authorization": "Token " + token,
        'User-Agent': 'rubick/v1.0'
    }

    time1 = time()
    if action == 'create_app':
        r = requests.request('POST', url, data=payload, headers=headers,
                             files=files)
    if action == 'delete_app':
        url = url + application_name + "/"
        r = requests.delete(url, headers=headers)
    if action == 'get_app':
        url = url + application_name
        r = requests.get(url, headers=headers)
    if action == 'start_app':
        url = url + application_name + "/start/"
        r = requests.put(url, headers=headers)
    if action == 'stop_app':
        url = url + application_name + "/stop/"
        r = requests.put(url, headers=headers)
    time2 = time()
    logger.debug("%s %s %s %d  time used: %f"
                 % (url, action, application_name,
                    r.status_code, time2 - time1))
    # print (url, payload, r.status_code, r.text)
    return (r.status_code, r.text)


def get_service_info(app_name, counter, namespace=settings.NAMESPACE):
    url = "{}v1/services/{}/{}".format(settings.API_URL, namespace, app_name)

    # time1 = time()
    r = requests.get(url, headers=headers)
    # time2 = time()
    # logger.debug("{} {} use {}s reutrns:{}".format(counter, url,
    #                                               time2 - time1,
    #                                               r.status_code))

    detail = {}
    if r.status_code >= 200 and r.status_code < 300:
        # transfer detail info to a dics
        obj = json.loads(r.text)
        if "current_status" not in obj:
            obj["current_status"] = "Stopped"
        if "current_num_instances" not in obj:
            obj["current_num_instances"] = 0
        detail = {
            "id": obj["uuid"],
            "service_name": obj["service_name"],
            "current_status": obj["current_status"],
            "image_name": obj["image_name"],
            "image_tag": obj["image_tag"],
            "current_num_instances": obj["current_num_instances"],
            "instance_size": obj["instance_size"]
        }

    # logger.debug(json.dumps(detail))
    return (r.status_code, r.text, detail)


def get_service_result(domain_name):
    url = "http://{}/".format(domain_name)
    try:
        r = requests.get(url)
        print("{} result {}:{}".format(url, r.status_code, r.text))
        return (r.status_code, r.text)
    except Exception as e:
        logger.info(e)
        return (500, "")


def is_deploying(service_name, target, maxcnt=120, namespace=settings.NAMESPACE):
    cnt = 0
    visit_500 = 0
    error_int = 0
    isdeploying = True
    while cnt < maxcnt and isdeploying:
        cnt = cnt + 1
        (code, text, obj) = get_service_info(service_name, cnt, namespace)
        logger.info(text)
        if code > 300:
            sleep(2)
            continue
        if "current_status" in obj and obj['current_status'].find('Error') >= 0:
            error_int = error_int + 1
            if error_int > 5:
                isdeploying = True
                break
            sleep(30)
            continue
        if "status" in target and target["status"] != obj["current_status"]:
            sleep(2)
            continue
        if "num" in target and target["num"] != obj["current_num_instances"]:
            sleep(2)
            continue
        if "tag" in target and target["tag"] != obj["image_tag"]:
            sleep(2)
            continue
        if "size" in target and target["size"] != obj["instance_size"]:
            sleep(2)
            continue
        if "text" in target:
            if visit_500 > 5:
                return True
            if "instance_ports" in json.loads(text) and len(json.loads(text)['instance_ports']):
                (code, html) = get_service_result(json.loads(text)["instance_ports"][0]["default_domain"])
                if code >= 300:
                    visit_500 = visit_500 + 1
                    sleep(30)
                    continue
                if target["text"] not in html:
                    visit_500 = visit_500 + 1
                    sleep(30)
                    continue
            elif "load_balancers" in json.loads(text) and len(json.loads(text)['load_balancers']):
                if json.loads(text)['load_balancers'][0]['is_internal']:
                    return False
                (code, html) = get_service_result(json.loads(text)["load_balancers"][0]["address"])
                print code, html
                if code >= 300:
                    visit_500 = visit_500 + 1
                    sleep(30)
                    continue
                if target["text"] not in html:
                    visit_500 = visit_500 + 1
                    sleep(30)
                    continue
            else:
                return False

        return False

    return isdeploying


def can_visit(target, text):
    if "instance_ports" in json.loads(text) and len(json.loads(text)['instance_ports']):
        (code, html) = get_service_result(json.loads(text)["instance_ports"][0]["default_domain"])
        if code >= 300:
            return False
        if target["text"] not in html:
            sleep(2)
            return False
    elif "load_balancers" in json.loads(text) and len(json.loads(text)['load_balancers']):
        if json.loads(text)['load_balancers'][0]['is_internal']:
            return True
        (code, html) = get_service_result(json.loads(text)["load_balancers"][0]["address"])
        print code, html
        if code >= 300:
            return False
        if target["text"] not in html:
            return False
    return True


def create_build_repo(repo_name, payload):
    time1 = time()
    logger.info('create auto build repo start')

    url = settings.API_URL + "v1/repositories/" + settings.NAMESPACE + "/"
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    print ("apicall returns (%d, %s)" % (r.status_code, r.text))
    logger.info("apicall returns (%d, %s)" % (r.status_code, r.text))
    if r.status_code < 200 or r.status_code > 300:
        return {"success": False, "code": r.status_code,
                "message": "failed in create repo, jakiro api error"}

    time2 = time()

    logger.info("create repo uses %f seconds" % (time2 - time1))
    obj = {
        "success": True,
        "total": time2 - time1
    }
    return obj


def get_build_info(build_id):
    url = settings.API_URL + "v1/builds/" + build_id
    status = 'W'
    cnt = 0
    while cnt < 600 and (status == 'I' or status == 'W'):
        # print cnt
        cnt = cnt + 1
        r = requests.get(url, headers=headers)
        if r.status_code < 200 or r.status_code >= 300:
            errmsg = "Failed in calling jakiro API: get trigger build({})"\
                .format(r.status_code)
            return {"success": False, "code": r.status_code, "message": errmsg}
        status = json.loads(r.text)['status']
        if status == 'S':
            return {"success": True, "status": status}
        elif status == 'F':
            return {"success": True, "status": status,
                    "message": "build trigger failed"}
        sleep(3)

    return {"success": False, "code": 1,
            "message": "Timeout in get trigger build"}


def trigger_build(repo_name):
    time1 = time()
    logger.info('trigger a build start')

    payload = {
        "tag": "latest",
        "namespace": settings.NAMESPACE,
        "repo_name": repo_name
    }
    url = settings.API_URL + "v1/builds/"
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    logger.info("trigger build returns(%d  %s)" % (r.status_code, r.text))

    if r.status_code < 200 or r.status_code > 300:
        return {"success": False, "code": r.status_code,
                "message": "Failed in trigger build"}

    build_id = json.loads(r.text)['build_id']

    resultSet = get_build_info(build_id)
    time2 = time()
    if resultSet['success']:
        return {
            "success": True,
            "total": time2 - time1
        }
    else:
        return resultSet


def send_email(subject, body, recipients):
    """class method to send an email"""

    # if settings.EMAIL is None or settings.SMTP is None:
    #    logger.error("No email/smtp config, email not sent.")
    #    return

    if not isinstance(recipients, list):
        raise TypeError(
            "{} should be a list".format(recipients))

    # we only support one sender for now
    from_email = EMAIL['from']

    # build message
    msg = MIMEMultipart()
    msg['From'] = from_email
    msg['To'] = ','.join(recipients)
    msg['Subject'] = Header(subject, 'utf8')
    msg.attach(MIMEText(body, 'html', 'utf8'))
    server = None
    try:
        server = smtplib.SMTP_SSL(host=SMTP['host'], port=SMTP['port'])
        server.set_debuglevel(SMTP['debug_level'])
        server.login(SMTP['username'], SMTP['password'])
        server.sendmail(from_email, recipients, msg.as_string())
        logger.info("send email successfully [%s -> %s: %s]" %
                    (msg['From'], msg['To'], subject))
        print "send email successfully"
    except Exception as e:
        # don't fatal if email was not send
        logger.info("send email exception [%s -> %s: %s]" %
                    (msg['From'], msg['To'], subject))
        logger.error(e)
        print "send email failed"
    finally:
        if server:
            server.quit()


if __name__ == "__main__":
    recipients = ["hchan@alauda.io"]
    send_email("subject", "body", recipients)
