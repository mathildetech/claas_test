class ServiceData(object):
   
    def __init__(self, service_name, namespace, region_name, mode=''):
        self.service_name = service_name
        self.namespace = namespace
        self.region_name = region_name
        self.mode = mode
    
    def haproxy_bridge_service(self):
        new_service = {
            "service_name": "Haproxybridge" + self.service_name,
            "health_checks": [],
            "image_name": "index.alauda.cn/alauda/hello-world",
            "image_tag": "latest",
            "instance_envvars": {},
            "instance_ports": [
                {
                   "container_port": 80,
                   "protocol": "tcp",
                   "endpoint_type": "http-endpoint"
                }
            ],
            "instance_size": "XXS",
            "linked_to_apps": "{}",
            "load_balancer_choice": "ENABLE",
            "load_balancers": [],
            "namespace": self.namespace,
            "network_mode": "BRIDGE",
            "region_name": self.region_name,
            "scaling_mode": "MANUAL",
            "service_mode": "SINGLE",
            "target_num_instances": 1,
            "target_state": "STARTED",
        }
        return new_service

    def haproxy_host_service(self):
        new_service = {
            "service_name": "Haproxyhost" + self.service_name,
            "namespace": self.namespace,
            "image_name": "index.alauda.cn/vipertest/e2e",
            "image_tag": "latest",
            "region_name": self.region_name,
            "instance_size": "XXS",
            "network_mode": "HOST",
            "scaling_mode": "MANUAL",
            "target_state": "STARTED",
            "target_num_instances": 1,
            "required_ports": [80],
            "instance_envvars": {"ENV": "TestEnv"},
            "load_balancers": [],
            "instance_ports": [
                {
                   "container_port": 80,
                   "protocol": "tcp",
                   "endpoint_type": "http-endpoint"
                }
            ],
        }
        return new_service

    def rawcontainer_bridge_service(self):
        new_service = {
            "service_name": "RB" + self.service_name,
            "namespace": self.namespace,
            "image_name": "index.alauda.cn/tutum/ubuntu",
            "image_tag": "latest",
            "region_name": self.region_name,
            "instance_size": "XXS",
            "network_mode": "BRIDGE",
            "scaling_mode": "MANUAL",
            "target_state": "STARTED",
            "target_num_instances": 1,
            "required_ports": [22],
            "load_balancers": [],
            "instance_ports":[]
        }
        return new_service
    
    def rawcontainer_host_service(self):
        new_service = {
            "service_name": "RH" + self.service_name,
            "namespace": self.namespace,
            "image_name": "index.alauda.cn/tutum/tomcat",
            "image_tag": "latest",
            "region_name": self.region_name,
            "instance_size": "XXS",
            "network_mode": "HOST",
            "scaling_mode": "MANUAL",
            "target_state": "STARTED",
            "target_num_instances": 1,
            "required_ports": [8080],
            "load_balancers": [],
            "instance_ports":[]         
        }
        return new_service
    
    def elb_host_internal_service(self):
        new_service = {
            "service_name": "EHI" + self.service_name,
            "namespace": self.namespace,
            "image_name": "index.alauda.cn/tutum/nginx",
            "image_tag": "latest",
            "region_name": self.region_name,
            "instance_size": "XS",
            "network_mode": "HOST",
            "scaling_mode": "MANUAL",
            "target_state": "STARTED",
            "target_num_instances": 1,
            "required_ports": [80],
            "load_balancers": [
                {
                  "type": self.mode,
                  "is_internal": "true",
                  "listeners": [
                        {
                          "protocol": "HTTP",
                          "lb_port": 80,
                          "container_port": 80
                        }
                    ]
                }
            ],
            "instance_ports":[]
        }
        return new_service
    
    def elb_host_external_service(self):
        new_service = {
            "service_name": "EHE" + self.service_name,
            "namespace": self.namespace,
            "image_name": "index.alauda.cn/tutum/nginx",
            "image_tag": "latest",
            "region_name": self.region_name,
            "instance_size": "XXS",
            "network_mode": "HOST",
            "scaling_mode": "MANUAL",
            "target_state": "STARTED",
            "target_num_instances": 1,
            "required_ports": [80],
            "instance_envvars": {"ENV": "TestEnv"},
            "load_balancers": [
                {
                    "type": self.mode,
                    "is_internal": False,
                    "listeners": [
                        {
                        "protocol": "HTTP",
                        "lb_port": 80,
                        "container_port": 80
                        }
                    ]
                }
            ],
            "instance_ports":[]
        }
        return new_service
    
    def elb_bridge_internal_service(self):
        new_service = {
            "service_name": "EBI" + self.service_name,
            "namespace": self.namespace,
            "image_name": "index.alauda.cn/tutum/nginx",
            "image_tag": "latest",
            "region_name": self.region_name,
            "instance_size": "XS",
            "network_mode": "BRIDGE",
            "scaling_mode": "MANUAL",
            "target_state": "STARTED",
            "target_num_instances": 1,
            "load_balancers": [
                {
                  "type": self.mode,
                  "is_internal": "true",
                  "listeners": [
                    {
                      "protocol": "HTTP",
                      "lb_port": 80,
                      "container_port": 80
                    }
                  ]
                }
            ],
            "instance_ports":[]
        }
        return new_service
    
    def elb_bridge_external_service(self):
        new_service = {
            "service_name": "EBE" + self.service_name,
            "namespace": self.namespace,
            "image_name": "index.alauda.cn/tutum/nginx",
            "image_tag": "latest",
            "region_name": self.region_name,
            "instance_size": "XXS",
            "network_mode": "BRIDGE",
            "scaling_mode": "MANUAL",
            "target_state": "STARTED",
            "target_num_instances": 1,
            "load_balancers": [
                {
                    "type": self.mode,
                    "is_internal": False,
                    "listeners": [
                        {
                            "protocol": "HTTP",
                            "lb_port": 80,
                            "container_port": 80
                        }
                    ]
                }
            ],
            "instance_ports":[]
        }
        return new_service

