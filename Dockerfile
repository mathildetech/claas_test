FROM ubuntu:14.04
MAINTAINER Yi Yuan <yiyuan@alauda.io>

COPY . /test

RUN apt-get update && \
    apt-get install -y python python-pip expect ssh

WORKDIR /test/testclaas

CMD ["python", "/test/testclaas/viper.py"]
